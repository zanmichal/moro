import React, { Component } from 'react';
import './App.css';
import Button from 'react-bootstrap/lib/Button';
import PropTypes from 'prop-types';

class UsersTableRemoveButton extends Component {

    constructor(props) {
        super(props);
        this.deleteRow = this.deleteRow.bind(this);
    }


    deleteRow() {
        console.log("delete " + this.props.index);
        this.props.onRowDelete(this.props.index);
    }

    render() {
        return (
            <Button
                bsStyle="danger"
                onClick={this.deleteRow}
                active
            >Remove</Button>
        );
    }
}

UsersTableRemoveButton.propTypes = {
    index: PropTypes.number,
    onRowDelete: PropTypes.func
};

export default UsersTableRemoveButton;