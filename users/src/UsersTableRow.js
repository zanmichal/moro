import React, { Component } from 'react';
import './App.css';
import UsersTableCell from './UsersTableCell';
import UserTableButtonsBar from './UserTableButtonsBar';
import PropTypes from "prop-types";

class UsersTableRow extends Component {
    render() {
        return (
            <tr>
                <UsersTableCell cellValue={this.props.user.id}/>
                <UsersTableCell cellValue={this.props.user.username}/>
                <UsersTableCell cellValue={this.props.user.firstName}/>
                <UsersTableCell cellValue={this.props.user.lastName}/>
                <UserTableButtonsBar
                    onRowDelete={(index) => this.props.onRowDelete(index)}
                    onShowModal={(index) => this.props.onShowModal(index)}
                    index={this.props.index}
                />
            </tr>
        );
    }
}

UsersTableRow.propTypes = {
    user: PropTypes.shape({
        id: PropTypes.number.isRequired,
        username: PropTypes.string.isRequired,
        firstName: PropTypes.string,
        lastName: PropTypes.string
    })
};

export default UsersTableRow;