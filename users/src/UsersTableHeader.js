import React, { Component } from 'react';
import './App.css';

class UsersTableHeader extends Component {
    render() {
        return (
            <thead>
                <tr>
                    <th>id</th>
                    <th>Username</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th></th>
                </tr>
            </thead>
        );
    }
}

export default UsersTableHeader;