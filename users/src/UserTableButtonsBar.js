import React, { Component } from 'react';
import './App.css';
import ButtonToolbar from "react-bootstrap/lib/ButtonToolbar";
import UsersTableRemoveButton from "./UsersTableRemoveButton";
import UsersTableModifyButton from "./UsersTableModifyButton";

class UserTableButtonsBar extends Component {

    render() {
        return (
            <td>
                <ButtonToolbar>
                    <UsersTableRemoveButton
                        onRowDelete={(index) => this.props.onRowDelete(index)}
                        index={this.props.index}
                    />
                    <UsersTableModifyButton
                        onShowModal={(index) => this.props.onShowModal(index)}
                        index={this.props.index}
                    />
                </ButtonToolbar>
            </td>
        );
    }
}

export default UserTableButtonsBar;