import React, { Component } from 'react';
import UsersTableHeader from './UsersTableHeader';
import UsersTableBody from './UsersTableBody';
import './App.css';
import UserForm from "./UserForm";
import Modal from 'react-bootstrap/lib/Modal';
import Panel from 'react-bootstrap/lib/Panel';

class UsersTable extends Component {

    constructor(props) {
        super(props);
        this.state = {show: false, loading: true, error: null, users: []};
        this.onRowDelete = this.onRowDelete.bind(this);
        this.onRowAdd = this.onRowAdd.bind(this);
        this.onShowModal = this.onShowModal.bind(this);
        this.onCancelModal = this.onCancelModal.bind(this);
        this.load = this.load.bind(this);
        this.handleState = this.handleState.bind(this);
    }

    componentDidMount() {
        this.load();
    }

    async load() {
        this.setState({ loading: true });
        const response = await fetch('/users');
        if (response.ok) {
            const body = await response.json();
            this.setState({ users: body });
        }
        this.setState({ loading: false });
    }

    async onRowDelete(index) {
        let user = this.state.users[index];
        const call = await fetch('/secured/user/' + user.id, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        });

        this.handleState(call);
    }

    async onRowAdd(username, firstName, lastName, password) {
        let user = {
            username: username,
            firstName: firstName,
            lastName: lastName,
            password: password
        };
        const call = await fetch('/user', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(user),
        });

        this.handleState(call);
    }

    async onModalConfirmed(username, firstName, lastName, password) {
        let user = this.state.users[this.state.modalIndex];
        user.username = username;
        user.firstName = firstName;
        user.lastName = lastName;
        user.password = password;
        const call = await fetch('/user', {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(user),
        });
        this.handleState(call);
        this.setState({show: false});
    }

    onShowModal(index) {
        this.setState({show: true, modalIndex:index});
    }

    onCancelModal() {
        this.setState({show: false});
    }

    handleState(call) {
        if (call.ok) {
            this.setState({error: null}, this.load);
        } else {
            this.setState({error: call.status + " - " + call.statusText});
        }
    }

    render() {
        if (this.state.loading) {
            return <p>Loading...</p>;
        }

        return (
            <div>
                <Panel id="error_panel" bsStyle="danger" expanded={this.state.error !== null}>
                    <Panel.Collapse>
                        <Panel.Body>
                            {this.state.error}
                        </Panel.Body>
                    </Panel.Collapse>
                </Panel>
                <UserForm
                    onConfirmed={(username, firstName, lastName, password) => this.onRowAdd(username, firstName, lastName, password)}
                    buttonText={'Add'}
                />
                <table>
                    <UsersTableHeader/>
                    <UsersTableBody
                        users={this.state.users}
                        onRowDelete={(index) => this.onRowDelete(index)}
                        onShowModal={(index) => this.onShowModal(index)}
                    />
                </table>
                <Modal show={this.state.show} onHide={this.onCancelModal}>
                    <Modal.Header closeButton>
                        <Modal.Title>Modify User</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <UserForm
                            onConfirmed={(username, firstName, lastName, password) => this.onModalConfirmed(username, firstName, lastName, password)}
                            user={this.state.users[this.state.modalIndex]}
                            buttonText={'Save'}
                        />
                    </Modal.Body>
                </Modal>
            </div>
        );
    }
}

export default UsersTable;
