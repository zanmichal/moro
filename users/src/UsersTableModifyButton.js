import React, { Component } from 'react';
import './App.css';
import Button from 'react-bootstrap/lib/Button';
import PropTypes from 'prop-types';
import UsersTableRemoveButton from "./UsersTableRemoveButton";

class UsersTableModifyButton extends Component {

    constructor(props) {
        super(props);
        this.modifyRow = this.modifyRow.bind(this);
    }

    modifyRow() {
        this.props.onShowModal(this.props.index);
    }

    render() {
        return (
            <Button
                onClick={this.modifyRow}
                bsStyle="info"
                active
            >Modify</Button>
        );
    }
}

UsersTableRemoveButton.propTypes = {
    index: PropTypes.number,
    onShowModal: PropTypes.func
};

export default UsersTableModifyButton;