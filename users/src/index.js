import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import UsersTable from './UsersTable';
import * as serviceWorker from './serviceWorker';

const USERS = [
    {id: 0, username: 'frantaBoss', firstName: 'František', lastName: 'Vopršálek'},
    {id: 1, username: 'B1GJO3', firstName: 'Josip Broz', lastName: 'Titto'},
    {id: 2, username: 'xenocide', firstName: 'Andrew', lastName: 'Wiggin'},
    {id: 3, username: 'stayAwhileAndListen', firstName: 'Decard', lastName: 'Cain'},
    {id: 4, username: 'theMagician', firstName: 'Anton', lastName: 'Gorodetsky'}
];

ReactDOM.render(<UsersTable users={USERS}/>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
