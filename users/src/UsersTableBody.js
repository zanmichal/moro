import React, { Component } from 'react';
import './App.css';
import UsersTableRow from './UsersTableRow';
import PropTypes from "prop-types";

class UsersTableBody extends Component {
    render() {
        const rows = [];

        for (let i = 0; i < this.props.users.length; i++) {
            rows.push(
                <UsersTableRow
                    user={this.props.users[i]}
                    onRowDelete={(index) => this.props.onRowDelete(index)}
                    onShowModal={(index) => this.props.onShowModal(index)}
                    index={i}
                    key={this.props.users[i].id}
                />
            );
        }

        return (
            <tbody>
                {rows}
            </tbody>
        );
    }
}

UsersTableBody.propTypes = {
    users: PropTypes.array
};

export default UsersTableBody;