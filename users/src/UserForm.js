import React, { Component } from 'react';
import FormGroup from "react-bootstrap/lib/FormGroup";
import FormControl from "react-bootstrap/lib/FormControl";
import Form from "react-bootstrap/lib/Form";
import Button from "react-bootstrap/lib/Button";
import PropTypes from "prop-types";

class UserForm extends Component {

    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

        this.state = {
            username: this.props.user === undefined ? '' : this.props.user.username,
            firstName: this.props.user === undefined ? '' : this.props.user.firstName,
            lastName: this.props.user === undefined ? '' : this.props.user.lastName,
            password: this.props.user === undefined ? '' : this.props.user.password
        };
    }

    getValidationState() {
        const usernameLength = this.state.username.length;
        const passwordLength = this.state.password.length;
        if (usernameLength > 2 && passwordLength > 2) {
            return 'success';
        }
        else if (usernameLength > 0 && passwordLength > 0) {
            return 'error';
        }
        return null;
    }

    handleChange(e) {
        let tempState = this.state;
        tempState[e.target.name] = e.target.value;
        this.setState( tempState );
    }

    handleSubmit() {
        this.props.onConfirmed(this.state.username, this.state.firstName, this.state.lastName, this.state.password);
    }

    render() {
        return (

            <Form inline>
                <FormGroup controlId="formUsername" validationState={this.getValidationState()}>
                    <FormControl type="text" name='username' placeholder="Username" value={this.state.username} onChange={this.handleChange}/>
                    <FormControl.Feedback />
                </FormGroup>{' '}
                <FormGroup controlId="formFirstName" validationState={this.getValidationState()}>
                    <FormControl type="text" name='firstName' placeholder="First Name" value={this.state.firstName} onChange={this.handleChange}/>
                </FormGroup>{' '}
                <FormGroup controlId="formLastName" validationState={this.getValidationState()}>
                    <FormControl type="text" name='lastName' placeholder="Last Name" value={this.state.lastName} onChange={this.handleChange}/>
                </FormGroup>{' '}
                <FormGroup controlId="formPassword" validationState={this.getValidationState()}>
                    <FormControl type="text" name='password' placeholder="Password" value={this.state.password} onChange={this.handleChange}/>
                </FormGroup>{' '}
                <Button bsStyle="primary"
                        disabled={this.getValidationState() === 'error'}
                        onClick={this.getValidationState() !== 'error' ? this.handleSubmit : null}>{this.props.buttonText}</Button>
            </Form>
        );
    }
}

UserForm.propTypes = {
    buttonText: PropTypes.string,
    onConfirmed: PropTypes.func,
    user: PropTypes.shape({
        id: PropTypes.number.isRequired,
        username: PropTypes.string.isRequired,
        firstName: PropTypes.string,
        lastName: PropTypes.string
    })
};

export default UserForm;