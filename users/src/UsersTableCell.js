import React, { Component } from 'react';
import './App.css';
import PropTypes from 'prop-types';

class UsersTableCell extends Component {
    render() {
        return (
            <td>{this.props.cellValue}</td>
        );
    }
}

UsersTableCell.propTypes = {
    cellValue: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ])
};

export default UsersTableCell;