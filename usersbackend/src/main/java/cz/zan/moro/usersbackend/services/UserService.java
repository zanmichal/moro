package cz.zan.moro.usersbackend.services;

import cz.zan.moro.usersbackend.model.User;
import cz.zan.moro.usersbackend.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * @author Michal Záň
 */
@Service
public class UserService implements UserDetailsService {

    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    private UserRepository repository;

    private static final String USER_AUTHORITY = "USER";

    @Autowired
    public UserService(UserRepository repository) {
        this.repository = repository;
    }


    public Collection<User> findAll() {
        return repository.findAllByOrderByIdAsc();
    }

    public Optional<User> findById(Long id) {
        return repository.findById(id);
    }

    public User save(User user) {
        return repository.save(user);
    }

    public void delete(Long id) {
        repository.deleteById(id);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = repository.findByUsername(username);
        if (user == null) {
            LOGGER.info("Could not find user " + username);
            throw new UsernameNotFoundException(username);
        }
        List<GrantedAuthority> authorities =  new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(USER_AUTHORITY));
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), authorities);
    }
}
