package cz.zan.moro.usersbackend.resources;

import cz.zan.moro.usersbackend.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Michal Záň
 */
@RestController
@RequestMapping("/secured")
public class UserSecuredResource {

    private final UserService service;

    @Autowired
    public UserSecuredResource(UserService service) {
        this.service = service;
    }

    @DeleteMapping("/user/{id}")
    public ResponseEntity<?> delete(@PathVariable long id) {
        service.delete(id);
        return ResponseEntity.ok().build();
    }

}
