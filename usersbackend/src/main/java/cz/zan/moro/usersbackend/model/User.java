package cz.zan.moro.usersbackend.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.lang.NonNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 * @author Michal Záň
 */
@Entity
@Table(name = "users")
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class User {

    @Id
    @Getter
    @Setter
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Getter
    @Setter
    @NonNull
    @Size(min = 3, max = 50)
    @Column(name="username", unique=true)
    private String username;

    @Getter
    @Setter
    @Size(max = 50)
    @Column(name="firstname")
    private String firstName;

    @Getter
    @Setter
    @Size(max = 50)
    @Column(name="lastname")
    private String lastName;

    @Getter
    @Setter
    @Size(min = 3, max = 259)
    @Column(name="password")
    private String password;

}
