package cz.zan.moro.usersbackend.resources;

import cz.zan.moro.usersbackend.model.User;
import cz.zan.moro.usersbackend.services.UserService;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;

/**
 * @author Michal Záň
 */
@RestController
public class UserResource {

    private final UserService service;

    @Autowired
    public UserResource(UserService service) {
        this.service = service;
    }

    @GetMapping("/users")
    Collection<User> users() {
        return service.findAll();
    }

    @GetMapping("/user/{id}")
    ResponseEntity<?> user(@PathVariable long id) {
        return service.findById(id)
                .map(response -> ResponseEntity.ok().body(response))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/user")
    ResponseEntity<User> create(@Valid @RequestBody User user) throws URISyntaxException {
        Logger.getLogger(this.getClass()).warn(user);
        User result = service.save(user);
        return ResponseEntity.created(new URI("/user/" + result.getId())).body(result);
    }

    @PutMapping("/user")
    ResponseEntity<User> update(@Valid @RequestBody User user) {
        User result = service.save(user);
        return ResponseEntity.ok().body(result);
    }

}
